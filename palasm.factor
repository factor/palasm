! pal assembler

USING: accessors arrays assocs bit-arrays io io.encodings.ascii io.files
  kernel namespaces math math.parser combinators
  sequences strings tools.continuations prettyprint ;

IN: palasm

TUPLE: pal inputs
      products array title part security
      genchip genfuse genpin fusecheck defaultfuse numfuses ;


TUPLE: pal-chip name inputs products ;
C: <pal-chip> pal-chip




SYMBOL: pinput

: pal-ip ( name -- ip )
  H{
    { "PAL10H8" { 32 64 } }
    { "PAL12H6" { 32 64 } }
    { "PAL14H4" { 32 64 } }
    { "PAL16H2" { 32 64 } }
    { "PAL16C1" { 32 64 } }
    { "PAL10L8" { 32 64 } }
    { "PAL12L6" { 32 64 } }
    { "PAL14L4" { 32 64 } }
    { "PAL16L2" { 32 64 } }
    { "PAL16L8" { 32 64 } }
    { "PAL16R8" { 32 64 } }
    { "PAL16R6" { 32 64 } }
    { "PAL16R4" { 32 64 } }
    { "PAL16X4" { 32 64 } }
    { "PAL16A4" { 32 64 } }
} at ;

! make and from values in pal
: pal-array ( pal -- array )
  [ pal? ] keep swap
  [
    [ products>> f <array> ] keep
    swap
    [
      drop
      [ inputs>> f <array> ] keep swap
    ] map
  ]
  [
    f 1array
  ] if swap drop ;

: pal-10H8 ( -- pal-chip )
  "PAL10H8" 32 64 <pal-chip> ;

! Pretty print the array
: pal-print ( pal -- )
  array>>
  [
    .
  ] each ;

: write-chip-file ( pal -- )
  "\n\n" write
  31 0x20 <string> write
  [ part>> ] keep swap [ string? ] keep swap
  [ "\n\n" append write ] [ drop "\n\n" write ] if
  26 0x20 <string> write
  "-------\\___/-------\n" write
  drop
  ;

: pal-write-chip-file ( filename pal -- )
  [ ascii ] dip [ break write-chip-file ] curry with-file-writer
  ;

! pal has a part number
: pal-part-number ( pn pal -- )
  [ pal? ] keep swap
  [ part<< ] [ drop drop ] if ;

! return the status of fuse check flag
: jedec-fusecheck? ( pal -- ? )
  fusecheck>> f = not ;

! test security bit and return JEDEC strings
: jedec-security-string ( pal -- $ )
  security>>
  [ "*G1" ] [ "*G0" ] if ;

! build string for jedec default value of fuses
: jedec-default-fuse ( pal -- $ )
  defaultfuse>> number>string "*F" prepend ;

! build string to print number of fuses
: jedec-number-fuses ( pal -- $ )
  numfuses>> number>string "*QF" prepend ;

! Pad number string to 5 places
: pad-00000 ( n -- string )
  number>string 5 48 pad-head ; inline

! JEDEC line address text
: jedec-address ( address -- string )
  pad-00000 "*L" prepend ;

: array-bit ( array -- string )
  [
    {
      { f [ "0" ] }
      { t [ "1" ] }
      [ drop "?" ]
    } case ] map concat ;


! create jedec
! pal data is turned into JEDED test array
: pal-jedec ( pal -- array )
  V{ } clone swap ! make an empty array of text
  [ jedec-fusecheck? ] keep swap
  [ [ [ "\002" swap push ] keep ] dip ] when
  [ [ "Used Program:  GALasm 4" swap push ] keep ] dip
  [ [ "GAL-Assembler:  GALasm 4" swap push ] keep ] dip
  [ break title>> "Device: " prepend swap push ] 2keep
  [ jedec-default-fuse swap push ] 2keep
  [ jedec-security-string swap push ] 2keep
  [ jedec-number-fuses swap push ] 2keep
  [ array>> ] keep swap
  [
    [ [ length ] keep swap ] dip *
    jedec-address
    " " append
    swap array-bit append
    swap [ swap [ push ] keep ] dip
  ] each-index
  drop ;


: <pal> ( palname -- pal )
  dup pal-ip first2
  pal new
  swap >>products swap >>inputs
  [ pal-array ] keep swap >>array
  swap >>title
  f >>security
  f >>genchip
  f >>genfuse
  f >>genpin
  f >>fusecheck
  0 >>defaultfuse
  0 >>numfuses ;
