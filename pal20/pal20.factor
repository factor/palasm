!


USING: namespaces io ;

IN: pal20


SYMBOLS: lun rpd roc pof pdf pms ifunct idesc lsame lact loperr linp lprd ;

: pal_start ( -- )
  "PALASM VERSION" print
  "WHAT IS THE LOGICAL UNIT NUMBER FOR OUTPUT(6)?:" print
  6 lun set
  1 rpd set
  5 roc set
  6 pof set
  6 pdf set
  10 pms set
  0 ifunct set
  0 idesc set

  f lsame set
  f lact set

  f loperr set
  f linp set

  f lprd set
  
  ;
